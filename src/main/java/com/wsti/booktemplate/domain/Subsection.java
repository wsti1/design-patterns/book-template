package com.wsti.booktemplate.domain;

public class Subsection extends SectionComponent {

    public Subsection(String title) {
        super(title);
    }

    @Override
    public void print(String pattern) {
        System.out.println(pattern + " " + title);
    }
}
