package com.wsti.booktemplate.domain;

import java.util.ArrayList;
import java.util.List;

public class CompositeSection extends SectionComponent {

    private List<SectionComponent> components;

    public CompositeSection(String title) {
        super(title);
        this.components = new ArrayList<>();
    }

    @Override
    public void add(SectionComponent component) {
        components.add(component);
    }

    @Override
    public void remove(SectionComponent component) {
        components.remove(component);
    }

    @Override
    public SectionComponent getComponent(int index) {
        return components.get(index);
    }

    @Override
    public void print(String pattern) {
        System.out.println(pattern + " " + title);
        components.forEach(component -> component.print(preparePattern(pattern, component)));
    }

    private String preparePattern(String pattern, SectionComponent component) {
        return "\t" + pattern + (components.indexOf(component) + 1) + ".";
    }
}
