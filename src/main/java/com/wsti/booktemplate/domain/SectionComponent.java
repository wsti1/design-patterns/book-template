package com.wsti.booktemplate.domain;

public abstract class SectionComponent {

    protected String title;

    public SectionComponent(String title) {
        this.title = title;
    }

    public abstract void print(String pattern);

    public void add(SectionComponent sectionComponent) {
        throw new UnsupportedOperationException();
    }

    public void remove(SectionComponent sectionComponent) {
        throw new UnsupportedOperationException();
    }

    public SectionComponent getComponent(int sectionComponent) {
        throw new UnsupportedOperationException();
    }
}
