package com.wsti.booktemplate;

import com.wsti.booktemplate.domain.CompositeSection;
import com.wsti.booktemplate.domain.SectionComponent;
import com.wsti.booktemplate.domain.Subsection;

public class BookTemplateApplication {

    public static void main(String[] args) {
        CompositeSection bookTemplate = createTemplate(4, 2, 2);
        bookTemplate.print("");
    }

    public static CompositeSection createTemplate(int chapterSize, int sectionSize, int subsectionsSize) {
        CompositeSection root = new CompositeSection("Table of Contents");
        addSections(root, "Chapter", chapterSize);

        for (int i = 0; i < chapterSize; i++) {
            CompositeSection chapter = (CompositeSection) root.getComponent(i);
            addSections(chapter, "Section", sectionSize);

            for (int j = 0; j < sectionSize; j++) {
                SectionComponent section = chapter.getComponent(j);
                addSubsections((CompositeSection) section, subsectionsSize);
            }
        }
        return root;
    }

    private static void addSections(CompositeSection section, String title, int size) {
        for (int i = 1; i <= size; i++) {
            CompositeSection nestedSection = new CompositeSection(title);
            section.add(nestedSection);
        }
    }

    private static void addSubsections(CompositeSection section, int size) {
        for (int i = 1; i <= size; i++) {
            section.add(new Subsection("Subsection"));
        }
    }
}
